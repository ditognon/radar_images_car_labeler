from torch.utils.data import Dataset
import numpy as np


class CustomDataset(Dataset):
	# initialize the constructor
    def __init__(self, images, labels, transforms=None, ):
        self.images = images
        self.transforms = transforms
        self.labels = labels
    

    def __getitem__(self, index):
        image = self.images[index]
        labels = list(map(lambda x: x.label, self.labels[index]))
        bboxes = list(map(lambda x: x.rect, self.labels[index]))

        image = np.transpose(image, (2, 0, 1))

        if self.transforms:
            image = self.transforms(image)

        return (image, labels, bboxes)

    def __len__(self):
        return len(self.images)