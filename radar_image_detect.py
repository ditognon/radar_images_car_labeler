from radar_image_label import RadarImageLabeler

import torch
import numpy as np

from torch.utils.data import DataLoader

r = RadarImageLabeler('data/images', 'data/radar_images')

r.process_images('car')
r.label_radar_images(verbose=True)
train = r.get_labeled_dataset()

train_loader = DataLoader(train, batch_size=10, shuffle=True)
train_images, train_labels, train_bboxes = next(iter(train_loader))

