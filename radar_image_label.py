import sys
import os

import torch

from PIL import Image
import numpy as np

import matplotlib.pyplot as plt

from custom_torch_dataset import CustomDataset

# Class used for representing labels
class Label:
    def __init__(self, rect, points):
        self.rect = rect
        self.points = points
        self.label = None

class Config:
    def __init__(self, filename=None):
        self.values = {}
        self.load_config_file("default_config.txt")
        if filename != None:
            self.load_config_file(filename)

    def load_config_file(self, filename):
        file = open(filename)

        for line in file.readlines():
            # Skip comments
            if line.startswith('#') or line == "\n":
                continue
            # Handle values
            else:
                temp = line.split("=")

                cond1 = len(temp[1].split(',')) > 1
                cond2 = (temp[1].startswith('true')) or (temp[1].startswith('false'))
                # regular float value
                if(cond1): 
                    colors_string = temp[1]
                    colors = colors_string.split(',')
                    self.values[temp[0]] = [int(i) for i in colors]
                elif(cond2):
                    if(temp[1].startswith('true')):
                        self.values[temp[0]] = True
                    else:
                        self.values[temp[0]] = False
                else:
                    self.values[temp[0]] = float(temp[1])

# Cuts out the excess part of the radar_image
def crop_image(image):
    photo_width = len(image[0])
    photo_height = len(image)
    
    start_x = int(config.values['radar_image_start_width_ratio'] * photo_width)
    start_y = int(config.values['radar_image_start_height_ratio'] * photo_height)
    end_x = int(config.values['radar_image_end_width_ratio'] * photo_width)
    end_y = int(config.values['radar_image_end_height_ratio'] * photo_height)

    cropped_image = image[start_y : end_y, start_x : end_x]
    return cropped_image

# Draws a red rectangle on image
def draw_rect(image, rect):
    for x in range(rect[0], rect[2] + 1):
        image[rect[1]][x][0] = 255
        image[rect[1]][x][1] = 0
        image[rect[1]][x][2] = 0
        image[rect[3]][x][0] = 255
        image[rect[3]][x][1] = 0
        image[rect[3]][x][2] = 0

    for y in range(rect[1], rect[3] + 1):
        image[y][rect[0]][0] = 255
        image[y][rect[0]][1] = 0
        image[y][rect[0]][2] = 0
        image[y][rect[2]][0] = 255
        image[y][rect[2]][1] = 0
        image[y][rect[2]][2] = 0

# Draws a red point on given image
def draw_point(image, point):
    image[point[1]][point[0]][0] = 255
    image[point[1]][point[0]][1] = 0
    image[point[1]][point[0]][2] = 0

class ObjectDetector:
    def __init__(self, config):
        self.config = config

    # Return a dict of information(with bounding boxes) for objects found in the image with class classname
    # key: image name
    # value: list of detections for that image with class = classname
    def detect_objects(self, classname, image_path, save_result_image=False):
        model = torch.hub.load('ultralytics/yolov5', 'yolov5m', pretrained=True)

        results = model(image_path)
        if save_result_image: results.save()
        
        pandas_detections = results.pandas().xyxy[0]
        img_detections = []
        for row in range(pandas_detections.shape[0]):
            bounding_box = pandas_detections.iloc[row, 0:4].tolist()
            res_class = pandas_detections.iloc[row, -1]
            probability = pandas_detections.iloc[row, -3]
            distance = self.dist_to_object(bounding_box)
            img_detections.append({"box": bounding_box, "class": res_class, "probability": probability, 'distance': distance})

        filtered_img_detections = filter(lambda object: object['class']== classname, img_detections)
        return list(filtered_img_detections)

    # Calculates distance to the object based on object width
    def dist_to_object(self, obj_box):
        return (self.config.values['focal'] * self.config.values['avg_object_width']) / (obj_box[2] - obj_box[0])

class ObjectLabeler:
    def __init__(self, config):
        self.config = config

    # Returns labels for a given radar_image
    def label_objects(self, radar_image, detections, possible_labels):
        labels = []
        if len(detections) > 0:
            for detection in detections:
                if self.config.values['verbose']: print(f'Searching radar image for detection: {detection["box"]}')
                found_obj = self.find_object(radar_image, detection['distance'], labels)
                if found_obj != None:
                    found_obj.label = detection['class']
                    labels.append(found_obj)
        else:
            cont = True
            while(cont):
                cont = False
                if self.config.values['verbose']: print(f'Searching radar image for objects.')
                found_obj = self.find_object(radar_image, 0, labels)
                if found_obj != None:
                    cont = True
                    if len(labels) <= 2:
                        found_obj.label = possible_labels[-(1 + len(labels))]
                    else:
                        found_obj.label = "ERROR"
                    labels.append(found_obj)

        if self.config.values['save_labeled_radar_images']:
            if self.config.values["points"]:
                for label in labels:
                    for point in label.points:
                        draw_point(radar_image, point)
            else:
                for label in labels:
                    draw_rect(radar_image, label.rect)

        plt.imshow(radar_image)
        plt.savefig(f'runs/label/{rimg_name[:-4]}_labeled.jpeg')

        return labels

    # Finds object in given radar_image
    # start_dist - distance from radar on which we start the search
    # min_width - min width of accepted objects, smaller objects will be discarded
    # min_length - min length of accepted objects, shorter objects will be discarded
    def find_object(self, radar_image, start_dist, found_objects):
        start_y = self.get_y_index_for_dist(radar_image, start_dist - self.config.values['dist_buffer'])
        end_y = self.get_y_index_for_dist(radar_image, start_dist + self.config.values['dist_buffer'])

        for y in range(start_y, end_y, -5):
            for x in range(len(radar_image[0]) - 1, -1, -5):
                if self.is_obj_color(radar_image[y][x]) and not self.in_objects(found_objects, x, y):
                    obj = True
                    is_obj, obj = self.explore_object(radar_image, x, y)
                    if is_obj:
                        if self.config.values['verbose']: print(f'Found obj: {obj.rect}')
                        return obj
                    else:
                        if self.config.values['verbose']: print(f'Found obj: {obj.rect} does not meet the minimum requirements')
        return None

    # Returns torch.utils.data.Dataset with labeled radar images
    def get_torch_dataset(self, labels):
        if labels != None:
            return CustomDataset(self.radar_images, labels)
        
        print('Please first run label_radar_images()')
        return None

    # Returns True if given color (r, g, b) is inside the range for confirming an object
    def is_confirm_color(self, color):
        for i in range(3):
            if color[i] < self.config.values['confirm_color_min'][i] or color[i] > self.config.values['confirm_color_max'][i]:
                return False
        return True

    # Returns True if given color (r, g, b) is inside the range for object colors
    def is_obj_color(self, color):
        for i in range(3):
            if color[i] < self.config.values['obj_color_min'][i] or color[i] > self.config.values['obj_color_max'][i]:
                return False
        return True

    # Checks whether a given point is already contained within some object from objects list
    def in_objects(self, objects, x, y):
        tolerance = config.values["in_objects_tolerance"]
        for obj in objects:
            if x >= (obj.rect[0] - tolerance) and x <= (obj.rect[2] + tolerance):
                if y >= (obj.rect[1] - tolerance) and y <= (obj.rect[3] + tolerance):
                    return True
        return False

        # Calculates y which equates to dist distance from the radar
    def get_y_index_for_dist(self, radar_image, dist):
        max_distance = self.config.values["max_distance"]
        image_height = len(radar_image)
        if dist < 0: return image_height - 1
        if dist >= max_distance: return 0

        res_y = int(((max_distance - dist) / max_distance) * image_height)

        return res_y

    # Returns rectangle coordinates (x1, y1, x2, y2) around given point in photo
    def explore_object(self, photo, x, y):
        res = [-1, -1, -1, y]
        points = []

        next_x = x
        obj_conf = False
        cont_y = True
        while(y >= 0 and cont_y):
            cont_y = False

            # Search left
            last_was_obj = True
            x = next_x
            while(x >= 0 and last_was_obj):
                last_was_obj = False
                if self.is_obj_color(photo[y][x]):
                    last_was_obj = True
                    if not cont_y and y >= 1 and self.is_obj_color(photo[y-1][x]):
                        cont_y = True
                        next_x = x
                    if res[0] == -1 or x < res[0]:
                        res[0] = x
                    if res[1] == -1 or y < res[1]:
                        res[1] = y
                    if self.is_confirm_color(photo[y][x]):
                        obj_conf = True

                    if (x, y) not in points:
                        points.append((x, y))

                x -= 1
            
            # Search right
            last_was_obj = True
            x = next_x + 1
            while(x < len(photo[0]) and last_was_obj):
                last_was_obj = False
                if self.is_obj_color(photo[y][x]):
                    last_was_obj = True
                    if not cont_y and y >= 1 and self.is_obj_color(photo[y-1][x]):
                        cont_y = True
                        next_x = x
                    if res[1] == -1 or y < res[1]:
                        res[1] = y
                    if res[2] == -1 or x > res[2]:
                        res[2] = x
                    if self.is_confirm_color(photo[y][x]):
                        obj_conf = True

                    if (x, y) not in points:
                        points.append((x, y))

                x += 1
            
            y -= 1

        # Check minimum object size
        min_width_pixels = (self.config.values['min_width'] / self.config.values['max_width']) * len(photo[0])
        min_length_pixels = (self.config.values['min_length'] / self.config.values['max_distance']) * len(photo)
        if ((res[2] - res[0]) < min_width_pixels) or ((res[3] - res[1]) < min_length_pixels): 
            obj_conf = False

        if self.config.values["known_positions"]:
            obj_conf = False
            for known_x in self.config.values["known_position_values"]:
                if (res[2] >= known_x) and (res[0] <= known_x):
                    obj_conf = True
                    break

        return obj_conf, Label(res, points)

if __name__ == '__main__':
    config_path = None
    images_folder_path = None
    radar_folder_path = None
    print(sys.argv)
    arg_i = 1
    while(arg_i < len(sys.argv)):
        if sys.argv[arg_i] == "-c":
            config_path = sys.argv[arg_i + 1]
        elif sys.argv[arg_i] == "-i":
            images_folder_path = sys.argv[arg_i + 1]
        elif sys.argv[arg_i] == "-r":
            radar_folder_path = sys.argv[arg_i + 1]

        arg_i += 2

    config = Config(config_path)

    if config.values["use_yolov5"]:
        # Load images
        all_detections = []
        image_names = []
        for i, img_name in enumerate(os.listdir(images_folder_path)):
            if(img_name.endswith(".jpg") or img_name.endswith(".png")):
                image_names.append(img_name)

                if config.values["verbose"]: print(f"Detecting objects for {img_name}")
                detector = ObjectDetector(config)
                detections = detector.detect_objects("car", f"{images_folder_path}/{img_name}", True)
                all_detections.append(detections)

    labeler = ObjectLabeler(config)

    # Load radar images and preprocess them
    all_labels = []
    radar_images = []
    radar_images_names = []
    index = 0
    for i, rimg_name in enumerate(os.listdir(radar_folder_path)):
        if(rimg_name.endswith(".jpg") or rimg_name.endswith(".png")):
            radar_image = np.asarray(Image.open(f'{radar_folder_path}/{rimg_name}')).copy()
            if config.values["crop"]:
                radar_image = crop_image(radar_image)
            if config.values["verbose"]: print(f"Labeling Radar image: {rimg_name}")
            possible_labels = []
            detections = []
            if config.values["use_yolov5"]:
                detections = all_detections[index]
            else:
                sr = rimg_name.split("_")
                for s in sr:
                    if s == '1300MHz':
                        break
                    possible_labels.append(s[1:])
            index += 1


            all_labels.append(labeler.label_objects(radar_image, detections, possible_labels))
            radar_images.append(radar_image)
            radar_images_names.append(rimg_name)

    

    if config.values["verbose"]: print("Tutto completo!")
